namespace CarRental
{
    public class CarRentException : System.Exception
    {
        public CarRentException() : base() { }
        public CarRentException(string message) : base(message) { }
        public CarRentException(string message, System.Exception inner) : base(message, inner) { }
        protected CarRentException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}