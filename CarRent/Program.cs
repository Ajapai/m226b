﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CarRental;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Newtonsoft.Json;


namespace CarRent
{
    class Program
    {
        static List<Vehicle> vehicleList = new List<Vehicle>();
        static void Main(string[] args)
        {
            bool finished = false;
            LoadData();

            while(!finished)
            {
                try
                {
                    Console.Clear();
                    DrawMenu();
                    DrawCarList();
                    Char key = Console.ReadKey(true).KeyChar;
                    switch (key)
                    {
                        case '1':
                            AddVehicle();
                            break;
                        
                        case '2':
                            DeleteCar();
                            break;

                        case '3':
                            SaveData();
                            finished = true;
                            break;

                        default:
                            throw new CarRentException();
                    }
                    
                }
                catch(Exception e)
                {
                    if(!File.Exists("car_Rent_logData.txt"))
                    {
                        File.Create("car_Rent_logData.txt");
                    }
                    var json = JsonConvert.SerializeObject(e, Formatting.Indented);
                    File.AppendAllText("car_Rent_logData.txt", json + "\n\n");
                    Console.Clear();
                    Console.WriteLine("Exception: " + e.GetType().Name);
                }
            }
        }


        static void DrawCarList()
        {
            Console.Write("vehicleList:\n");
            foreach (Vehicle vehicle in vehicleList)
            {
                Console.Write(vehicle.Name + " - ");
                if(vehicle is Car car)
                {
                    Console.Write(car.CarCompany + "\n");
                }
                else if(vehicle is Truck truck)
                {
                    Console.Write(truck.TruckCompany + "\n");
                }
            }
        }

        static void DrawMenu()
        {
            Console.Write("[1] - Add Vehicle.\n[2] - Delete Vehicle.\n[3] - Exit Programm\n\n");
        }

        static void AddVehicle()
        {
            Console.Clear();
            Console.WriteLine("Please enter a Vehicle name: ");
            string vehicleName = Console.ReadLine();
            Console.WriteLine("Please enter a company name: ");
            string companyName = Console.ReadLine();
            Console.WriteLine("What type of vehicle is it?\n[0] - Car\n[1] - Truck");

            bool validKey = false;
            while(!validKey)
            {
                Char key = Console.ReadKey(true).KeyChar;
                switch (key)
                {
                    case '0':
                        vehicleList.Add(new Car(){Name = vehicleName, CarCompany = companyName});
                        validKey = true;
                        break;

                    case '1':
                        vehicleList.Add(new Truck(){Name = vehicleName, TruckCompany = companyName});
                        validKey = true;
                        break;

                    default:
                        Console.WriteLine("Please choose a valid vehicle Type:\n[0] - Car\n[1] - Truck");
                        break;
                }
            }
        }

        static void DeleteCar()
        {
            Console.Clear();
            Console.Write("What Vehicle do you want to delete?\n");
            for (int i = 0; i < vehicleList.Count; i++)
            {
                Console.WriteLine("[" + i + "] - " + vehicleList[i].Name);
            }
            int choosenCar = Convert.ToInt32(Console.ReadLine());
            vehicleList.Remove(vehicleList[choosenCar]);
        }

        static void SaveData()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings()
            {
                TypeNameHandling = TypeNameHandling.Auto
            };
            var json = JsonConvert.SerializeObject(vehicleList, settings);
            File.WriteAllText("carRent_saveData.txt", json);
        }

        static void LoadData()
        {
            if(File.Exists("carRent_saveData.txt"))
            {
                JsonSerializerSettings settings = new JsonSerializerSettings()
                {
                    TypeNameHandling = TypeNameHandling.Auto
                };
                var json = File.ReadAllText("carRent_saveData.txt");
                vehicleList = JsonConvert.DeserializeObject<List<Vehicle>>(json, settings);
            }
        }
    }
}
