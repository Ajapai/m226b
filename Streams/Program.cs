﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Streams
{
    class Program
    {
        static void Main(string[] args)
        {
            using (FileStream fs = new FileStream("myFileStreamFile.txt", FileMode.Create))
            {
                byte[] byteArray = {100, 112, 125};
                fs.Write(byteArray, 0, byteArray.Length);
                byte[] readArray = new byte[byteArray.Length];
                fs.Position = 0;
                fs.Read(readArray, 0, byteArray.Length);
                using (StreamWriter sw = new StreamWriter(fs, Encoding.Default))
                {
                    sw.Write("Yeet");
                }
                for(int i = 0; i < readArray.Length; i++)
                {
                    Console.Write(readArray[i] + "\n");
                }
            }
        }
    }
}
